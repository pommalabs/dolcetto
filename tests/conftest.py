# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import pytest

from dolcetto.testing import register_hypothesis_profiles

register_hypothesis_profiles()


@pytest.fixture
def anyio_backend():
    return "asyncio"
