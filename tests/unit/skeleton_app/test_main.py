# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import cast

import pytest
from litestar.middleware import DefineMiddleware
from litestar.testing import AsyncTestClient, TestClient

from dolcetto import ApiKeyAuthenticationMiddleware
from dolcetto.event_handlers import warn_if_security_disabled
from dolcetto.openapi import operation_id_creator
from skeleton_app.main import app, public_api_v1, restricted_api_v1


def test_that_app_can_be_loaded():
    # Act
    with TestClient(app=app) as client:
        # Assert
        assert client


def test_that_public_api_v1_router_does_not_enforce_authentication():
    # Assert
    assert not any(
        filter(
            lambda m: cast(DefineMiddleware, m).middleware
            is ApiKeyAuthenticationMiddleware,
            public_api_v1.middleware,
        )
    )


def test_that_restricted_api_v1_router_enforces_authentication():
    # Assert
    assert any(
        filter(
            lambda m: cast(DefineMiddleware, m).middleware
            is ApiKeyAuthenticationMiddleware,
            restricted_api_v1.middleware,
        )
    )


def test_that_app_has_expected_startup_handlers():
    # Assert
    assert len(app.on_startup) == 1
    assert warn_if_security_disabled in app.on_startup


def test_that_app_has_expected_shutdown_handlers():
    # Assert
    assert len(app.on_shutdown) == 0


@pytest.mark.anyio
async def test_that_app_exposes_openapi_definition_built_with_custom_operation_id_creator():
    # Act
    async with AsyncTestClient(app=app) as client:
        # Assert
        assert app.openapi_config is not None
        assert app.openapi_config.operation_id_creator is operation_id_creator
        response = await client.get("/schema/openapi.json")
        assert "AppInfoApi_checkAppHealth" in response.text
        assert "AppInfoApi_getAppVersion" in response.text
        assert "FairUseTokenApiV1_getFairUseToken" in response.text
        assert "TodoApiV1_getTodoItems" in response.text
