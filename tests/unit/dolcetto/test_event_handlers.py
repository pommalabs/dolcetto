# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging

import pytest
from hypothesis import given

from dolcetto.event_handlers import warn_if_security_disabled
from dolcetto.settings import ApiKey, SecuritySettings
from dolcetto.testing import capture_logs_at_level
from tests.utils import valid_api_keys_strategy

################################################################################
# Startup
################################################################################


def test_that_warn_if_security_disabled_logs_when_no_api_key_has_been_configured(
    request: pytest.FixtureRequest,
):
    # Arrange
    with capture_logs_at_level(request, logging.WARNING) as caplog:
        security_settings = SecuritySettings(api_keys=frozenset())
        # Act
        warn_if_security_disabled(security_settings=security_settings)
        # Assert
        assert len(caplog.messages) == 1
        assert "unauthenticated" in caplog.messages[0]


@given(valid_api_keys=valid_api_keys_strategy())
def test_that_warn_if_security_disabled_does_not_log_when_api_keys_has_been_configured(
    valid_api_keys: list[ApiKey], request: pytest.FixtureRequest
):
    # Arrange
    with capture_logs_at_level(request, logging.WARNING) as caplog:
        security_settings = SecuritySettings(api_keys=frozenset(valid_api_keys))
        # Act
        warn_if_security_disabled(security_settings=security_settings)
        # Assert
        assert len(caplog.messages) == 0
