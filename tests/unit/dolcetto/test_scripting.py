# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import string
from unittest.mock import patch

import pytest
from hypothesis import given
from hypothesis import strategies as st

import dolcetto.scripting
from dolcetto.scripting import (
    generate_fair_use_token_key,
    git_version,
    start,
)
from dolcetto.scripting import (
    test as _test,  # Avoids that pytest considers it a test and tries to run it.
)
from dolcetto.services import FairUseTokenService


@st.composite
def _module_strategy(draw: st.DrawFn):
    return draw(st.text(alphabet=string.ascii_lowercase, min_size=32))


@pytest.mark.parametrize(
    "git_describe, auto_increment, expected_version",
    [
        ("1", True, "1.0+abcd1234"),
        ("1-23-xxxxxxxx", True, "1.23+abcd1234"),
        ("1.2.3", False, "1.2.3+abcd1234"),
        ("1.2.3-pre", False, "1.2.3-pre+abcd1234"),
        ("1.2.3-xx-yyyyyyyy", False, "1.2.3+abcd1234"),
        ("1.2.3-pre-xx-yyyyyyyy", False, "1.2.3-pre+abcd1234"),
    ],
)
def test_that_git_version_produces_expected_version(
    git_describe: str, auto_increment: bool, expected_version: str
):
    # Arrange
    counter = 0

    def cmd_output(_):
        nonlocal counter
        if counter == 0:
            counter += 1
            return git_describe
        commit_sha = "abcd1234"
        return commit_sha

    with (
        patch.object(dolcetto.scripting, "cmd_output", side_effect=cmd_output),
        patch.object(dolcetto.scripting, "cmd_call") as mock,
    ):
        # Act
        git_version(auto_increment)
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        version = command.split(" ", maxsplit=3)[2]
        assert version == expected_version


@given(_module_strategy())
def test_that_start_invokes_uvicorn(module: str):
    # Arrange
    with patch.object(dolcetto.scripting, "cmd_call") as mock:
        # Act
        start(module)
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        assert "uvicorn" in command
        assert module in command


def test_that_test_invokes_pytest():
    # Arrange
    with patch.object(dolcetto.scripting, "cmd_call") as mock:
        # Act
        _test()
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        assert "pytest" in command


@given(st.lists(_module_strategy()))
def test_that_test_configures_specified_modules_for_code_coverage(modules: list[str]):
    # Arrange
    with patch.object(dolcetto.scripting, "cmd_call") as mock:
        # Act
        _test(*modules)
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        for module in modules:
            assert module in command


def test_that_generate_fair_use_token_key_prints_the_key(
    capsys: pytest.CaptureFixture[str],
):
    # Act
    generate_fair_use_token_key()
    # Assert
    output = capsys.readouterr().out
    assert len(output) == (FairUseTokenService.TOKEN_KEY_LENGTH + len(os.linesep))
