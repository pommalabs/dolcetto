# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from ipaddress import IPv4Address
from unittest.mock import MagicMock, patch

import httpx
import pytest
from healthcheck import HealthCheck
from hypothesis import given
from hypothesis import strategies as st
from litestar.status_codes import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_503_SERVICE_UNAVAILABLE,
)
from pydantic import AnyHttpUrl

from dolcetto.services import get_logger, probe_url
from dolcetto.testing import capture_logs_at_level

PROBE_TIMEOUT = 10


@given(ip_address=st.ip_addresses(v=4))
def test_that_health_check_returns_200_when_probe_url_check_succeeds(
    ip_address: IPv4Address, request: pytest.FixtureRequest
):
    # Arrange
    url = f"https://{ip_address}"
    logger = get_logger()
    health_check = HealthCheck(
        checkers=[lambda: probe_url(AnyHttpUrl(url), logger, PROBE_TIMEOUT)]
    )
    with (
        patch.object(httpx, "head"),
        capture_logs_at_level(request, logging.WARNING) as caplog,
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_200_OK
        assert len(caplog.messages) == 0


@given(ip_address=st.ip_addresses(v=4))
def test_that_health_check_returns_503_when_probe_url_check_fails_to_connect(
    ip_address: IPv4Address, request: pytest.FixtureRequest
):
    # Arrange
    url = f"https://{ip_address}"
    logger = get_logger()
    health_check = HealthCheck(
        failed_status=HTTP_503_SERVICE_UNAVAILABLE,
        checkers=[lambda: probe_url(AnyHttpUrl(url), logger, PROBE_TIMEOUT)],
    )
    with (
        patch.object(httpx, "head", side_effect=httpx.HTTPError(str(url))),
        capture_logs_at_level(request, logging.WARNING) as caplog,
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_503_SERVICE_UNAVAILABLE
        assert len(caplog.messages) == 2
        assert url in caplog.messages[0]


@given(ip_address=st.ip_addresses(v=4))
def test_that_health_check_returns_503_when_probe_url_check_receives_error_code(
    ip_address: IPv4Address, request: pytest.FixtureRequest
):
    # Arrange
    url = f"https://{ip_address}"
    logger = get_logger()
    health_check = HealthCheck(
        failed_status=HTTP_503_SERVICE_UNAVAILABLE,
        checkers=[lambda: probe_url(AnyHttpUrl(url), logger, PROBE_TIMEOUT)],
    )
    ko_response = MagicMock()
    ko_response_status_code = HTTP_500_INTERNAL_SERVER_ERROR
    ko_response.raise_for_status.side_effect = httpx.HTTPStatusError(
        str(url),
        request=httpx.Request("HEAD", str(url)),
        response=httpx.Response(ko_response_status_code),
    )
    with (
        patch.object(httpx, "head", return_value=ko_response),
        capture_logs_at_level(request, logging.WARNING) as caplog,
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_503_SERVICE_UNAVAILABLE
        assert len(caplog.messages) == 2
        assert url in caplog.messages[0]
        assert str(ko_response_status_code) in caplog.messages[0]
