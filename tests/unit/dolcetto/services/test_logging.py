# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from unittest.mock import MagicMock

from litestar.status_codes import HTTP_500_INTERNAL_SERVER_ERROR
from litestar.testing import create_test_client

from dolcetto import logging_exception_handler
from dolcetto.services import get_logging_config
from dolcetto.settings import AppInsightsSettings
from tests.utils import (
    RAISE_NAME_ERROR_ENDPOINT,
    STUB_APPINSIGHTS_CONNECTION_STRING,
    STUB_APPINSIGHTS_INSTRUMENTATION_KEY,
    raise_name_error,
)

OTEL_LOG_HANDLER = "opentelemetry"


def test_that_get_logging_config_adds_opentelemetry_handler_when_app_insights_connection_string_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=STUB_APPINSIGHTS_CONNECTION_STRING, instrumentationkey=None
    )
    # Act
    logging_config = get_logging_config(appinsights_settings)
    # Assert
    assert OTEL_LOG_HANDLER in logging_config.handlers
    assert OTEL_LOG_HANDLER in logging_config.loggers["app"]["handlers"]


def test_that_get_logging_config_adds_opentelemetry_handler_when_app_insights_instrumentation_key_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=None, instrumentationkey=STUB_APPINSIGHTS_INSTRUMENTATION_KEY
    )
    # Act
    logging_config = get_logging_config(appinsights_settings)
    # Assert
    assert OTEL_LOG_HANDLER in logging_config.handlers
    assert OTEL_LOG_HANDLER in logging_config.loggers["app"]["handlers"]


def test_that_logger_does_not_contain_opentelemetry_handler_when_app_insights_connection_string_is_not_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=None, instrumentationkey=None
    )
    # Act
    logging_config = get_logging_config(appinsights_settings)
    # Assert
    assert OTEL_LOG_HANDLER not in logging_config.handlers
    assert OTEL_LOG_HANDLER not in logging_config.loggers["app"]["handlers"]


def test_that_logging_exception_handler_logs_raised_exception():
    # Arrange
    mock = MagicMock(spec_set=logging.Logger)
    with create_test_client(
        route_handlers=raise_name_error,
        exception_handlers={
            # A mock is used here, instead of caplog, because Litestar logging config
            # interferes with caplog itself and log records are not tracked.
            HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler(lambda: mock)
        },
    ) as client:
        # Act
        response = client.get(RAISE_NAME_ERROR_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_500_INTERNAL_SERVER_ERROR
        assert "Internal Server Error" in response.text
        mock.exception.assert_called_once()
        assert isinstance(mock.exception.call_args.kwargs["exc_info"], NameError)
