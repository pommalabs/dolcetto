# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.


from hypothesis import given
from hypothesis import strategies as st

from dolcetto.models import FairUseToken
from dolcetto.services import FairUseTokenService
from dolcetto.settings import SecuritySettings


def _fair_use_token_service_factory():
    token_key = FairUseTokenService.generate_token_key()
    security_settings = SecuritySettings(fair_use_token_key=token_key)
    return FairUseTokenService(security_settings)


def test_that_fair_use_token_service_generates_non_empty_token():
    # Arrange
    fair_use_token_service = _fair_use_token_service_factory()
    # Act
    token = fair_use_token_service.generate_token()
    # Assert
    assert len(token.token) > 0


def test_that_fair_use_token_service_validates_newly_generated_token():
    # Arrange
    fair_use_token_service = _fair_use_token_service_factory()
    fair_use_token = fair_use_token_service.generate_token()
    # Act
    is_valid = fair_use_token_service.token_is_valid(fair_use_token)
    # Assert
    assert is_valid


@given(st.text())
def test_that_fair_use_token_service_does_not_validate_token_with_invalid_encoding(
    invalid_token: str,
):
    # Arrange
    fair_use_token_service = _fair_use_token_service_factory()
    fair_use_token = FairUseToken(token=invalid_token)
    # Act
    is_valid = fair_use_token_service.token_is_valid(fair_use_token)
    # Assert
    assert not is_valid


def test_that_fair_use_token_service_does_not_validate_token_with_invalid_encryption():
    # Arrange
    fair_use_token_service = _fair_use_token_service_factory()
    another_fair_use_token_service = _fair_use_token_service_factory()
    fair_use_token = another_fair_use_token_service.generate_token()
    # Act
    is_valid = fair_use_token_service.token_is_valid(fair_use_token)
    # Assert
    assert not is_valid


def test_that_fair_use_token_service_does_not_validate_token_sent_twice():
    # Arrange
    fair_use_token_service = _fair_use_token_service_factory()
    fair_use_token = fair_use_token_service.generate_token()
    # Act
    fair_use_token_service.token_is_valid(fair_use_token)
    is_valid = fair_use_token_service.token_is_valid(fair_use_token)
    # Assert
    assert not is_valid
