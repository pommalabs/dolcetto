# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from dolcetto import APP_DESCRIPTION, APP_VERSION

################################################################################
# App information
################################################################################


def test_that_app_description_is_read_from_pyproject_file():
    # Assert
    assert len(APP_DESCRIPTION) > 0


def test_that_app_version_is_read_from_pyproject_file():
    # Assert
    assert len(APP_VERSION) > 0
