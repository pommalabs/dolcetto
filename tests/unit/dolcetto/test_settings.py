# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
from uuid import UUID

import pytest
from hypothesis import given
from hypothesis import strategies as st

from dolcetto.settings import AppInsightsSettings

################################################################################
# AppInsightsSettings
################################################################################


@given(st.text(alphabet=string.ascii_letters, min_size=1))
def test_that_get_connection_string_returns_connection_string_when_provided(
    conn_str: str,
):
    # Arrange
    settings = AppInsightsSettings(connection_string=conn_str, instrumentationkey=None)
    # Act
    has_conn_str = settings.has_connection_string()
    result_conn_str = settings.get_connection_string()
    # Assert
    assert has_conn_str
    assert result_conn_str == conn_str


@given(
    st.text(alphabet=string.ascii_letters, min_size=1),
    st.uuids(allow_nil=True),
)
def test_that_get_connection_string_returns_connection_string_first_when_both_provided(
    conn_str: str,
    instr_key: UUID,
):
    # Arrange
    settings = AppInsightsSettings(
        connection_string=conn_str, instrumentationkey=instr_key
    )
    # Act
    has_conn_str = settings.has_connection_string()
    result_conn_str = settings.get_connection_string()
    # Assert
    assert has_conn_str
    assert result_conn_str == conn_str


@given(st.uuids(allow_nil=True))
def test_that_get_connection_string_returns_instrumentation_key_when_provided(
    instr_key: UUID,
):
    # Arrange
    settings = AppInsightsSettings(connection_string=None, instrumentationkey=instr_key)
    # Act
    has_conn_str = settings.has_connection_string()
    result_conn_str = settings.get_connection_string()
    # Assert
    assert has_conn_str
    assert str(instr_key) in result_conn_str


def test_that_get_connection_string_raises_error_when_nothing_provided():
    # Arrange
    settings = AppInsightsSettings(connection_string=None, instrumentationkey=None)
    # Act
    has_conn_str = settings.has_connection_string()
    with pytest.raises(ValueError) as ex_info:
        settings.get_connection_string()
    # Assert
    assert not has_conn_str
    assert "connection string" in str(ex_info.value)
    assert "instrumentation key" in str(ex_info.value)
