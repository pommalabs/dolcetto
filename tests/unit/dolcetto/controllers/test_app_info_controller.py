# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import MagicMock

from healthcheck import HealthCheck
from hypothesis import given
from hypothesis import strategies as st
from litestar.di import Provide
from litestar.status_codes import HTTP_200_OK, HTTP_503_SERVICE_UNAVAILABLE
from litestar.testing import create_test_client
from polyfactory.factories.pydantic_factory import ModelFactory

from dolcetto import APP_VERSION, AppInfoController
from dolcetto.models import HealthCheckResponse


class HealthCheckResponseFactory(ModelFactory[HealthCheckResponse]):
    __model__ = HealthCheckResponse


@given(
    st.builds(HealthCheckResponseFactory.build),
    st.sampled_from([HTTP_200_OK, HTTP_503_SERVICE_UNAVAILABLE]),
)
def test_that_check_app_health_endpoint_triggers_health_checks(
    result: HealthCheckResponse, status_code: int
):
    # Arrange
    health_check = MagicMock(spec_set=HealthCheck)
    health_check.run = MagicMock(
        return_value=(result.model_dump_json(), status_code, None)
    )
    with create_test_client(
        route_handlers=[AppInfoController],
        dependencies={
            "health_check": Provide(lambda: health_check, sync_to_thread=False)
        },
    ) as client:
        # Act
        response = client.get("/health")
        # Assert
        assert response.status_code == status_code
        assert response.json() == result.model_dump()


def test_that_get_app_version_endpoint_responds_with_app_version():
    # Arrange
    with create_test_client(route_handlers=AppInfoController) as client:
        # Act
        response = client.get("/version")
        # Assert
        assert response.status_code == HTTP_200_OK
        assert response.json()["version"] == APP_VERSION
