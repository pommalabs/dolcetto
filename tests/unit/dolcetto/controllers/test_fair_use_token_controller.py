# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar.di import Provide
from litestar.status_codes import HTTP_200_OK
from litestar.testing import create_test_client

from dolcetto.controllers import FairUseTokenController
from dolcetto.services import FairUseTokenService
from dolcetto.settings import SecuritySettings


def test_that_get_fair_use_token_responds_with_fair_use_token():
    # Arrange
    with create_test_client(
        route_handlers=FairUseTokenController,
        dependencies={
            "security_settings": Provide(SecuritySettings.load, sync_to_thread=False),
            "fair_use_token_svc": Provide(FairUseTokenService, sync_to_thread=False),
        },
    ) as client:
        # Act
        response = client.get("/fair-use-token")
        # Assert
        assert response.status_code == HTTP_200_OK
        assert len(response.json()["token"]) > 0
