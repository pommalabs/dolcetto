# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import random
from typing import Callable
from unittest.mock import MagicMock, patch

from hypothesis import given
from hypothesis import strategies as st
from litestar.status_codes import (
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED,
    HTTP_500_INTERNAL_SERVER_ERROR,
)
from litestar.testing import create_test_client
from opentelemetry.sdk.trace.export import SpanExporter
from opentelemetry.sdk.trace.sampling import Sampler

from dolcetto import (
    API_KEY_HEADER_NAME,
    ApiKeyAuthenticationMiddleware,
    AppInfoController,
    HttpTracingMiddleware,
)
from dolcetto.settings import ApiKey, SecuritySettings
from tests.utils import (
    RAISE_NAME_ERROR_ENDPOINT,
    api_key_strategy,
    expired_api_keys_strategy,
    raise_name_error,
    valid_api_keys_strategy,
)

GET_APP_VERSION_ENDPOINT = "/version"

################################################################################
# ApiKeyAuthenticationMiddleware
################################################################################


@given(valid_api_keys_strategy())
def test_that_api_key_auth_responds_401_when_security_is_enabled_and_api_key_has_not_been_sent(
    valid_api_keys: list[ApiKey],
):
    # Arrange
    security_settings = SecuritySettings(api_keys=frozenset(valid_api_keys))
    with create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            ApiKeyAuthenticationMiddleware.define(security_settings=security_settings)
        ],
    ) as client:
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_401_UNAUTHORIZED


@given(valid_api_keys_strategy(), st.data())
def test_that_api_key_auth_responds_401_when_security_is_enabled_and_api_key_is_invalid(
    valid_api_keys: list[ApiKey], data: Callable[..., st.SearchStrategy[st.DataObject]]
):
    # Arrange
    invalid_api_key = data.draw(
        api_key_strategy().filter(
            lambda ak: ak.value not in [v.value for v in valid_api_keys]
        )
    )
    security_settings = SecuritySettings(api_keys=frozenset(valid_api_keys))
    with create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            ApiKeyAuthenticationMiddleware.define(security_settings=security_settings)
        ],
    ) as client:
        client.headers[API_KEY_HEADER_NAME] = invalid_api_key.value
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_401_UNAUTHORIZED


@given(expired_api_keys_strategy())
def test_that_api_key_auth_responds_200_when_security_is_enabled_and_api_key_has_expired(
    expired_api_keys: list[ApiKey],
):
    # Arrange
    expired_api_key = random.choice(expired_api_keys)
    security_settings = SecuritySettings(api_keys=frozenset(expired_api_keys))
    with create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            ApiKeyAuthenticationMiddleware.define(security_settings=security_settings)
        ],
    ) as client:
        client.headers[API_KEY_HEADER_NAME] = expired_api_key.value
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_401_UNAUTHORIZED


@given(valid_api_keys_strategy())
def test_that_api_key_auth_responds_200_when_security_is_enabled_and_api_key_is_valid(
    valid_api_keys: list[ApiKey],
):
    # Arrange
    valid_api_key = random.choice(valid_api_keys)
    security_settings = SecuritySettings(api_keys=frozenset(valid_api_keys))
    with create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            ApiKeyAuthenticationMiddleware.define(security_settings=security_settings)
        ],
    ) as client:
        client.headers[API_KEY_HEADER_NAME] = valid_api_key.value
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_200_OK


def test_that_api_key_auth_responds_200_when_security_is_disabled_and_api_key_has_not_been_sent():
    # Arrange
    security_settings = SecuritySettings(api_keys=frozenset())
    with create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            ApiKeyAuthenticationMiddleware.define(security_settings=security_settings)
        ],
    ) as client:
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_200_OK


@given(api_key_strategy())
def test_that_api_key_auth_responds_200_when_security_is_disabled_and_api_key_has_been_sent(
    api_key: ApiKey,
):
    # Arrange
    security_settings = SecuritySettings(api_keys=frozenset())
    with create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            ApiKeyAuthenticationMiddleware.define(security_settings=security_settings)
        ],
    ) as client:
        client.headers[API_KEY_HEADER_NAME] = api_key.value
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_200_OK


################################################################################
# HttpTracingMiddleware
################################################################################


def test_that_http_tracing_traces_200_response_status_code():
    # Arrange
    sampler = MagicMock(spec_set=Sampler)
    exporter = MagicMock(spec_set=SpanExporter)
    with patch.object(
        HttpTracingMiddleware, "_add_request_attributes"
    ) as add_request_attributes, patch.object(
        HttpTracingMiddleware, "_add_response_attributes"
    ) as add_response_attributes, create_test_client(
        route_handlers=AppInfoController,
        middleware=[HttpTracingMiddleware.define(sampler, exporter)],
    ) as client:
        # Act
        response = client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_200_OK
        add_request_attributes.assert_called_once()
        add_response_attributes.assert_called_once()


def test_that_http_tracing_traces_500_response_status_code():
    # Arrange
    sampler = MagicMock(spec_set=Sampler)
    exporter = MagicMock(spec_set=SpanExporter)
    with patch.object(
        HttpTracingMiddleware, "_add_request_attributes"
    ) as add_request_attributes, patch.object(
        HttpTracingMiddleware, "_add_response_attributes"
    ) as add_response_attributes, create_test_client(
        route_handlers=raise_name_error,
        middleware=[HttpTracingMiddleware.define(sampler, exporter)],
    ) as client:
        # Act
        response = client.get(RAISE_NAME_ERROR_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_500_INTERNAL_SERVER_ERROR
        add_request_attributes.assert_called_once()
        add_response_attributes.assert_called_once()


def test_that_http_tracing_traces_respects_request_filter():
    # Arrange
    sampler = MagicMock(spec_set=Sampler)
    exporter = MagicMock(spec_set=SpanExporter)
    with patch.object(
        HttpTracingMiddleware, "_add_request_attributes"
    ) as add_request_attributes, patch.object(
        HttpTracingMiddleware, "_add_response_attributes"
    ) as add_response_attributes, create_test_client(
        route_handlers=AppInfoController,
        middleware=[
            HttpTracingMiddleware.define(
                sampler,
                exporter,
                request_filter=lambda r: r.url.path != GET_APP_VERSION_ENDPOINT,
            )
        ],
    ) as client:
        # Act
        client.get(GET_APP_VERSION_ENDPOINT)
        # Assert
        add_request_attributes.assert_not_called()
        add_response_attributes.assert_not_called()
