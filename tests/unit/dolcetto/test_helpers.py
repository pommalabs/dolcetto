# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import pytest

from dolcetto.helpers import camelize


@pytest.mark.parametrize(
    ("to_be_camelized", "expected"),
    [
        ("", ""),
        ("_", ""),
        ("enabled", "enabled"),
        ("user_name", "userName"),
    ],
)
def test_that_camelize_works_as_expected(to_be_camelized: str, expected: str):
    # Act
    result = camelize(to_be_camelized)
    # Assert
    assert result == expected
