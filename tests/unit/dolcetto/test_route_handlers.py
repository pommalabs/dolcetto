# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import pytest
from litestar.status_codes import HTTP_302_FOUND
from litestar.testing import create_test_client

from dolcetto import redirect_to_docs


@pytest.mark.parametrize("endpoint", ["/", "/docs"])
def test_that_root_and_docs_endpoints_redirect_to_openapi_docs(endpoint):
    # Arrange
    with create_test_client(route_handlers=redirect_to_docs) as client:
        # Act
        response = client.get(endpoint, follow_redirects=False)
        # Assert
        assert response.status_code == HTTP_302_FOUND
        assert response.has_redirect_location
        assert response.headers["location"].endswith("/schema/swagger")
