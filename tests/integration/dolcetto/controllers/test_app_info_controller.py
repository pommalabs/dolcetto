# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import uuid
from pathlib import Path
from unittest.mock import patch

import pyrfc6266
import pytest
from litestar.status_codes import HTTP_200_OK
from litestar.testing import create_test_client

import tests
from dolcetto import AppInfoController


@pytest.fixture(name="source_code_archive", scope="module")
def fixture_source_code_archive():
    archive_name = str(uuid.uuid4()) + ".zip"
    archive_path = Path(tests.__file__).parent.parent / archive_name
    archive_contents = uuid.uuid4().bytes
    with archive_path.open(mode="wb") as archive_file:
        archive_file.write(archive_contents)
    yield (archive_name, archive_contents)
    archive_path.unlink()


def test_that_get_app_source_code_endpoint_responds_with_source_code_archive(
    source_code_archive,
):
    # Arrange
    (archive_name, archive_contents) = source_code_archive
    with create_test_client(route_handlers=AppInfoController) as client:
        # Act
        with patch(
            "dolcetto.controllers.app_info_controller._SOURCE_CODE_ARCHIVE_NAME",
            archive_name,
        ):
            response = client.get("/source-code")
        # Assert
        assert response.status_code == HTTP_200_OK
        content_disposition_header = response.headers.get("Content-Disposition")
        assert pyrfc6266.parse_filename(content_disposition_header) == archive_name
        assert response.content == archive_contents
