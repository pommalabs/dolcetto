# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
from datetime import datetime, timedelta
from uuid import UUID

from hypothesis import strategies as st
from litestar import get

from dolcetto.settings import ApiKey

STUB_APPINSIGHTS_CONNECTION_STRING = f"InstrumentationKey={UUID(int=0)}"
STUB_APPINSIGHTS_INSTRUMENTATION_KEY = UUID(int=0)

RAISE_NAME_ERROR_ENDPOINT = "/raise"

MAX_TEXT_SIZE = 8
MAX_LIST_SIZE = 3

_DATETIME_NOW = datetime.now()


@get(RAISE_NAME_ERROR_ENDPOINT)
async def raise_name_error() -> None:
    raise NameError()


@st.composite
def api_key_strategy(draw: st.DrawFn, min_expiry=datetime.min, max_expiry=datetime.max):
    return draw(
        st.builds(
            ApiKey,
            name=st.text(
                alphabet=string.ascii_letters, min_size=1, max_size=MAX_TEXT_SIZE
            ),
            value=st.text(
                alphabet=string.ascii_letters, min_size=1, max_size=MAX_TEXT_SIZE
            ),
            expires_at=st.one_of(
                st.none(),
                st.datetimes(
                    timezones=st.timezones(),
                    min_value=min_expiry,
                    max_value=max_expiry,
                    allow_imaginary=False,
                ),
            ),
        )
    )


@st.composite
def valid_api_keys_strategy(draw: st.DrawFn):
    min_expiry = _DATETIME_NOW + timedelta(days=1)
    return draw(
        st.lists(
            api_key_strategy(min_expiry=min_expiry),
            min_size=1,
            max_size=MAX_LIST_SIZE,
        )
    )


@st.composite
def expired_api_keys_strategy(draw: st.DrawFn):
    max_expiry = _DATETIME_NOW - timedelta(days=1)
    return draw(
        st.lists(
            api_key_strategy(max_expiry=max_expiry).filter(
                lambda ak: ak.expires_at is not None
            ),
            min_size=1,
            max_size=MAX_LIST_SIZE,
        )
    )
