# Dolcetto

Shared components (route handlers, middleware, etc.) used for Litestar applications.

## License

MIT © 2022-2024 [PommaLabs Team and Contributors][pommalabs-website]

[pommalabs-website]: https://pommalabs.xyz/
