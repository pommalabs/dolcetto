# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar import Controller, get


class TodoController(Controller):
    path = "/todo"
    tags = ["todo"]

    @get(path="/")
    async def get_todo_items(self) -> list[int]:
        return []
