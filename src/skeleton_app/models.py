# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from pydantic import BaseModel, Field


class WeatherForecastEntry(BaseModel):
    temp_c: float = Field()


class WeatherForecast(BaseModel):
    hourly: list[WeatherForecastEntry] = Field()
