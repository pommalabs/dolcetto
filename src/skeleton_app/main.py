# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar import Litestar, Router, get, post
from litestar.di import Provide
from litestar.exceptions import NotAuthorizedException
from litestar.openapi import OpenAPIConfig
from litestar.openapi.spec import Components
from litestar.status_codes import HTTP_500_INTERNAL_SERVER_ERROR

from dolcetto import (
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    MIT_LICENSE,
    ApiKeyAuthenticationMiddleware,
    AppInfoController,
    HttpTracingMiddleware,
    logging_exception_handler,
    on_app_init,
    redirect_to_docs,
)
from dolcetto.controllers import FairUseTokenController
from dolcetto.models import MailMessage
from dolcetto.services import SmtpClient
from skeleton_app.controllers import TodoController
from skeleton_app.models import WeatherForecast
from skeleton_app.services import WeatherForecastService, get_health_check

root = Router(
    path="/",
    route_handlers=[redirect_to_docs, AppInfoController],
    dependencies={
        "health_check": Provide(get_health_check, use_cache=True, sync_to_thread=False),
    },
)


@get(
    "/weather-forecast",
    tags=["weather_forecast"],
    raises=[NotAuthorizedException],
    summary="Returns a random weather forecast.",
)
async def get_weather_forecast(
    weather_forecast_svc: WeatherForecastService,
) -> WeatherForecast:
    return weather_forecast_svc.get_forecast()


@post(
    "/send-mail",
    tags=["mail_sender"],
    raises=[NotAuthorizedException],
    summary="Sends a mail message.",
)
async def send_mail(
    smtp_client: SmtpClient | None,
    data: MailMessage,
) -> None:
    if smtp_client is None:
        return
    with smtp_client.open_connection() as smtp_conn:
        smtp_conn.send_mail(data)


public_api_v1 = Router(
    path="/api/v1",
    route_handlers=[FairUseTokenController],
    middleware=[HttpTracingMiddleware.define()],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
)

restricted_api_v1 = Router(
    path="/api/v1",
    route_handlers=[get_weather_forecast, send_mail, TodoController],
    security=[{API_KEY_SECURITY_SCHEME_NAME: []}],
    middleware=[
        ApiKeyAuthenticationMiddleware.define(),
        HttpTracingMiddleware.define(),
    ],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
    dependencies={
        "weather_forecast_svc": Provide(WeatherForecastService, sync_to_thread=False),
    },
)

app = Litestar(
    openapi_config=OpenAPIConfig(
        title="Skeleton App",
        description=APP_DESCRIPTION,
        version=APP_VERSION,
        license=MIT_LICENSE,
        components=Components(
            security_schemes={API_KEY_SECURITY_SCHEME_NAME: API_KEY_SECURITY_SCHEME}
        ),
    ),
    route_handlers=[root, public_api_v1, restricted_api_v1],
    on_app_init=[on_app_init],
)
