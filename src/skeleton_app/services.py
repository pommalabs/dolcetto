# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import logging
import random

from healthcheck import HealthCheck
from pydantic import AnyHttpUrl

from dolcetto.services import probe_url
from skeleton_app.models import WeatherForecast


def randomly_failing_check() -> tuple[bool, str]:
    success = random.random() < 0.5  # nosec B311
    return (success, "OK" if success else "KO")


def probe_example_com(logger: logging.Logger) -> tuple[bool, str]:
    return probe_url(AnyHttpUrl("https://www.example.com"), logger, timeout=10)


def get_health_check(logger: logging.Logger) -> HealthCheck:
    health_check = HealthCheck()

    health_check.add_check(randomly_failing_check)

    check = functools.partial(probe_example_com, logger)
    setattr(check, "__name__", probe_example_com.__name__)
    health_check.add_check(check)

    return health_check


class WeatherForecastService:
    def __init__(self, logger: logging.Logger):
        self._logger = logger

    def get_forecast(self) -> WeatherForecast:
        self._logger.info("Forecast request received")
        return WeatherForecast(hourly=[])
