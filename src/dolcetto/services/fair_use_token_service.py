# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import uuid

from cachetools import TTLCache
from cryptography.fernet import Fernet, InvalidToken

from dolcetto.models import FairUseToken
from dolcetto.settings import SecuritySettings


class FairUseTokenService:
    TOKEN_KEY_LENGTH = 44

    def __init__(self, security_settings: SecuritySettings):
        self._security_settings = security_settings
        self._fernet = FairUseTokenService._create_fernet(self._security_settings)
        # Following cache contains UUIDs, enveloped in fair use tokens,
        # which have already been validated and that should be blocked if sent again.
        # This mechanism is simple, performant and it does not rely on external services
        # or persistent stores, but it has many weaknesses.
        # The first one is that the cache has an upper bound in order to avoid flooding,
        # but that upper bound, once reached, would make used tokens valid again,
        # at least until their expiration.
        # The second one is that the cache is stored in memory, which means that
        # in a load balancing scenario, without any server/pod affinity logic,
        # each token could be used at least once per server/pod.
        self._used_uuid_cache: TTLCache = TTLCache(
            maxsize=self._security_settings.fair_use_token_cache_size,
            ttl=self._security_settings.fair_use_token_ttl,
        )

    @staticmethod
    def generate_token_key() -> str:
        return Fernet.generate_key().decode()

    def generate_token(self) -> FairUseToken:
        random_uuid = uuid.uuid4()
        token = self._fernet.encrypt(random_uuid.bytes)
        return FairUseToken(token=token.decode())

    def token_is_valid(self, fair_use_token: FairUseToken) -> bool:
        try:
            random_uuid = uuid.UUID(
                bytes=self._fernet.decrypt(
                    fair_use_token.token,
                    self._security_settings.fair_use_token_ttl,
                )
            )
        except ValueError:
            # Fair use token encoding was not valid.
            return False
        except InvalidToken:
            # Fair use token encryption was not valid.
            return False
        if self._used_uuid_cache.get(random_uuid):
            # UUID has already been used.
            return False
        self._used_uuid_cache[random_uuid] = True
        return True

    @staticmethod
    def _create_fernet(security_settings: SecuritySettings) -> Fernet:
        if security_settings.fair_use_token_key is not None:
            return Fernet(str(security_settings.fair_use_token_key))
        # Fair use token key has not been specified,
        # a random key should be generated and used instead.
        return Fernet(Fernet.generate_key())
