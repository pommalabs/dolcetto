# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from typing import Any, Callable

from azure.monitor.opentelemetry.exporter import AzureMonitorLogExporter
from litestar import Request, Response
from litestar.exceptions.responses import create_exception_response
from litestar.logging import LoggingConfig
from opentelemetry._logs import get_logger_provider, set_logger_provider
from opentelemetry.sdk._logs import LoggerProvider
from opentelemetry.sdk._logs.export import BatchLogRecordProcessor

from dolcetto.settings import AppInsightsSettings


def get_logging_config(
    appinsights_settings: AppInsightsSettings | None = None,
) -> LoggingConfig:
    appinsights_settings = appinsights_settings or AppInsightsSettings.load()
    handler_keys = []
    handlers: dict[str, dict[str, Any]] = {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "standard",
        },
    }

    if appinsights_settings.has_connection_string():
        configure_azure_monitor_log_exporter(appinsights_settings)
        handler_keys.append("opentelemetry")
        handlers["opentelemetry"] = {
            "class": "opentelemetry.sdk._logs.LoggingHandler",
            "level": "INFO",
        }

    return LoggingConfig(
        loggers={
            "app": {"level": "INFO", "handlers": handler_keys},
            "httpx": {"level": "WARNING", "handlers": handler_keys},
            "litestar": {"level": "INFO", "handlers": handler_keys},
            # Log messages produced by Azure Monitor should not be sent
            # to any handler, except default queue listener.
            "azure": {"level": "WARNING", "handlers": []},
        },
        handlers=handlers,
    )


def configure_azure_monitor_log_exporter(
    appinsights_settings: AppInsightsSettings,
) -> None:
    if isinstance(get_logger_provider(), LoggerProvider):
        return

    logger_provider = LoggerProvider()
    set_logger_provider(logger_provider)
    exporter = AzureMonitorLogExporter.from_connection_string(
        appinsights_settings.get_connection_string()
    )
    logger_provider.add_log_record_processor(BatchLogRecordProcessor(exporter))


def get_logger() -> logging.Logger:
    return logging.getLogger("app")


def logging_exception_handler(
    logger_factory: Callable[..., logging.Logger] = get_logger,
) -> Callable[[Request, Exception], Response]:
    def handler(request: Request, exc: Exception) -> Response:
        logger = logger_factory()
        logger.exception("Unhandled exception", exc_info=exc)
        return create_exception_response(request, exc)

    return handler
