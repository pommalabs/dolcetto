# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging

import backoff
import httpx
from pydantic import AnyHttpUrl


def probe_url(
    url: AnyHttpUrl,
    logger: logging.Logger,
    timeout: float,
) -> tuple[bool, str]:
    """Returns True if specified URL responds to an HTTP HEAD request."""

    # Performs an HEAD request to specified URL, retrying it when
    # a transport error occurs. Other kind of errors should not be retried.
    @backoff.on_exception(backoff.expo, httpx.TransportError, max_tries=3)
    def perform_head_request() -> None:
        response = httpx.head(str(url), timeout=timeout, follow_redirects=True)
        response.raise_for_status()

    try:
        perform_head_request()
        return (True, "OK")
    except httpx.HTTPStatusError as err:
        logger.warning(
            'Probe for URL "%s" received %d. Response headers: %s',
            url,
            err.response.status_code,
            err.response.headers,
        )
    except httpx.HTTPError as err:
        logger.warning('Probe for URL "%s" failed', url, exc_info=err)

    # If we got here, then the probe failed.
    return (False, "KO")
