# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import Any

from azure.monitor.opentelemetry.exporter import AzureMonitorTraceExporter
from opentelemetry.sdk.trace.export import SpanExporter, SpanExportResult
from opentelemetry.sdk.trace.sampling import ALWAYS_OFF, ALWAYS_ON, Sampler

from dolcetto.settings import AppInsightsSettings


class DummyExporter(SpanExporter):
    def export(self, _: Any) -> SpanExportResult:
        # Dummy exporter does nothing with incoming span data.
        return SpanExportResult.SUCCESS


def get_sampler(
    appinsights_settings: AppInsightsSettings | None = None,
) -> Sampler:
    appinsights_settings = appinsights_settings or AppInsightsSettings.load()
    if appinsights_settings.has_connection_string():
        return ALWAYS_ON
    return ALWAYS_OFF


def get_exporter(
    appinsights_settings: AppInsightsSettings | None = None,
) -> SpanExporter:
    appinsights_settings = appinsights_settings or AppInsightsSettings.load()
    if appinsights_settings.has_connection_string():
        connection_string = appinsights_settings.get_connection_string()
        return AzureMonitorTraceExporter(connection_string=connection_string)
    return DummyExporter()
