# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import itertools
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from functools import cache
from smtplib import SMTP, SMTP_SSL, SMTPException
from typing import Any, Optional, Self

from dolcetto.models import MailMessage
from dolcetto.settings import SmtpConnectionSecurity, SmtpSettings


class SmtpClient:
    def __init__(self, smtp_settings: SmtpSettings | None):
        self._settings = smtp_settings or SmtpSettings.load()

    @staticmethod
    @cache
    def create() -> Optional["SmtpClient"]:
        try:
            return SmtpClient(SmtpSettings.load())
        except ValueError:
            return None

    @property
    def settings(self) -> SmtpSettings:
        return self._settings

    def open_connection(self) -> "SmtpConnection":
        return SmtpConnection(self._settings)


class SmtpConnection:
    def __init__(self, smtp_settings: SmtpSettings):
        self._settings = smtp_settings
        self._smtp: SMTP | None = None
        self._logged_in = False

    @property
    def settings(self) -> SmtpSettings:
        return self._settings

    def __enter__(self) -> Self:
        if self._settings.connection_security is SmtpConnectionSecurity.SSL:
            self._smtp = self._open_ssl_encrypted_connection()
        elif self._settings.connection_security is SmtpConnectionSecurity.STARTTLS:
            self._smtp = self._open_starttls_encrypted_connection()
        else:
            self._smtp = self._open_unencrypted_connection()
        return self

    def __exit__(self, *_: Any) -> None:
        if self._smtp is not None:
            self._smtp.close()

    def send_mail(self, mail_message: MailMessage) -> None:
        from_addr = (
            str(mail_message.from_address)
            if mail_message.from_address is not None
            # Default sender should be used when none has been specified.
            else str(self._settings.default_from_address)
        )
        to_addrs = [
            str(addr)
            for addr in itertools.chain(
                self._settings.default_to_addresses, mail_message.to_addresses
            )
        ]

        msg = MIMEMultipart()
        msg["Subject"] = mail_message.subject
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg.preamble = mail_message.subject

        if mail_message.plain_text_content is not None:
            msg.attach(MIMEText(mail_message.plain_text_content, "plain"))
        if mail_message.html_content is not None:
            msg.attach(MIMEText(mail_message.html_content, "html"))

        self._perform_login_if_needed()
        self._send_mail(from_addr, to_addrs, msg)

    def _open_ssl_encrypted_connection(self) -> SMTP_SSL:
        context = ssl.create_default_context()
        return SMTP_SSL(
            str(self._settings.server), self._settings.port, context=context
        )

    def _open_starttls_encrypted_connection(self) -> SMTP:
        context = ssl.create_default_context()
        smtp = SMTP(str(self._settings.server), self._settings.port)
        try:
            smtp.starttls(context=context)
            return smtp
        except:
            smtp.close()
            raise

    def _open_unencrypted_connection(self) -> SMTP:
        return SMTP(str(self._settings.server), self._settings.port)

    def _get_smtp(self) -> SMTP:
        if self._smtp is None:
            # SMTP connection has not been initialized properly.
            raise SMTPException()
        return self._smtp

    def _perform_login_if_needed(self) -> None:
        if self._logged_in:
            # Login has already been performed on this connection.
            return
        if self._settings.user_name is not None:
            # Login should be performed only when user has been specified.
            smtp = self._get_smtp()
            smtp.login(self._settings.user_name, self._settings.password or "")
        # Set login as performed even if not really done, in order to make next calls,
        # if any, no-op on the same code path.
        self._logged_in = True

    def _send_mail(
        self, from_addr: str, to_addrs: list[str], msg: MIMEMultipart
    ) -> None:
        smtp = self._get_smtp()
        smtp.sendmail(from_addr, to_addrs, msg.as_string())
