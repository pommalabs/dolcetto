# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar import Controller, get

from dolcetto.models import FairUseToken
from dolcetto.services import FairUseTokenService


class FairUseTokenController(Controller):
    path = "/fair-use-token"
    tags = ["fair_use_token"]

    @get("/", summary="Generates and returns an encrypted fair use token.")
    async def get_fair_use_token(
        self, fair_use_token_svc: FairUseTokenService
    ) -> FairUseToken:
        return fair_use_token_svc.generate_token()
