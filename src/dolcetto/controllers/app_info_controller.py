# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from pathlib import Path

from healthcheck import HealthCheck
from litestar import Controller, Request, Response, get
from litestar.response import File

from dolcetto import constants
from dolcetto.models import AppVersion, HealthCheckResponse

_APP_VERSION = AppVersion(version=constants.APP_VERSION)
_SOURCE_CODE_ARCHIVE_NAME = "source-code.zip"


class AppInfoController(Controller):
    path = "/"
    tags = ["app_info"]

    @get("/health", summary="Checks application health.")
    async def check_app_health(
        self, request: Request, health_check: HealthCheck
    ) -> Response[HealthCheckResponse]:
        message, status_code, _ = health_check.run()
        return Response(
            HealthCheckResponse.model_validate_json(message),
            status_code=status_code,
            type_encoders=request.app.type_encoders,
        )

    @get("/version", summary="Returns application version.")
    async def get_app_version(self) -> AppVersion:
        return _APP_VERSION

    @get(
        "/source-code",
        summary="Returns a ZIP archive containing application source code.",
        media_type="application/zip",
    )
    async def get_app_source_code(self) -> File:
        archive_path = Path.cwd() / _SOURCE_CODE_ARCHIVE_NAME
        return File(archive_path, filename=_SOURCE_CODE_ARCHIVE_NAME)
