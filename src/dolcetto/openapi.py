# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar.handlers import HTTPRouteHandler
from litestar.openapi.spec import License, SecurityScheme
from litestar.types import Method
from litestar.types.internal_types import PathParameterDefinition

from dolcetto.constants import API_KEY_HEADER_NAME
from dolcetto.helpers import SEPARATORS_CLEANUP_PATTERN, camelize

API_KEY_SECURITY_SCHEME_NAME = "api_key"
API_KEY_SECURITY_SCHEME = SecurityScheme(
    type="apiKey",
    name=API_KEY_HEADER_NAME,
    security_scheme_in="header",
)

MIT_LICENSE = License(name="MIT", url="https://opensource.org/licenses/MIT")


def operation_id_creator(
    route_handler: HTTPRouteHandler,
    _: Method,
    path_components: list[str | PathParameterDefinition],
) -> str:
    """Creates a unique 'operationId' for an OpenAPI PathItem entry.

    Args:
        route_handler: The HTTP Route Handler instance.
        http_method: The HTTP method for the given PathItem.
        path_components: A list of path components.

    Returns:
        An operationId created from the handler function name,
        prefixed by an API namespace created from API version and first tag.
    """

    operation_name = camelize(route_handler.handler_name)

    route_tag = "Root"
    for layer in route_handler.ownership_layers:
        if layer.tags is not None and len(layer.tags) >= 1:
            route_tag = layer.tags[0].title()
            break

    route_version = (
        str(path_components[1]).upper()
        if len(path_components) >= 2 and path_components[0] == "api"
        else ""
    )

    operation_namespace = f"{route_tag}Api{route_version}"
    operation_namespace = SEPARATORS_CLEANUP_PATTERN.sub("", operation_namespace)

    return f"{operation_namespace}_{operation_name}"
