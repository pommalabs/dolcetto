# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import sys
from subprocess import CalledProcessError, check_call, check_output  # nosec

from dolcetto.services import FairUseTokenService

DEFAULT_PORT = "8080"


def cmd_call(command: str) -> None:
    try:
        # Command is used for scripting and build automation, it is not an input
        # received from the user or a command which contains user defined parameters.
        check_call(command, shell=True)  # nosec
    except CalledProcessError as err:
        sys.exit(err.returncode)


def cmd_output(command: str) -> str:
    try:
        # Command is used for scripting and build automation, it is not an input
        # received from the user or a command which contains user defined parameters.
        return check_output(command, shell=True, text=True)  # nosec
    except CalledProcessError as err:
        sys.exit(err.returncode)


def git_version(auto_increment: bool = False) -> None:
    parts = cmd_output("git describe --tags").strip().split("-", maxsplit=4)
    commit_sha = cmd_output("git rev-parse --short=8 HEAD").strip()
    if auto_increment:
        # Handled cases:
        # x             -> 1 part, tag is applied to current commit.
        # x-height-sha  -> 3 parts, tag has been applied on a previous commit.
        version_number = parts[0]
        git_height = parts[1] if len(parts) > 1 else "0"
        version = f"{version_number}.{git_height}+{commit_sha}"
    else:
        # Handled cases:
        # x.y.z                -> 1 part, tag is applied to current commit.
        # x.y.z-pre            -> 2 parts, pre-release tag is applied to current commit.
        # x.y.z-height-sha     -> 3 parts, tag has been applied on a previous commit.
        # x.y.z-pre-height-sha -> 4 parts, pre-release tag has been applied on a previous commit.
        version_number = parts[0] if len(parts) % 2 == 1 else "-".join(parts[:2])
        version = f"{version_number}+{commit_sha}"
    cmd_call(f"poetry version {version}")


def start(module: str) -> None:
    port = os.getenv("PORT", DEFAULT_PORT)
    cmd_call(
        f"uvicorn {module}.main:app --port {port} --reload"
        " --no-access-log --no-server-header"
    )


def test(*modules: str) -> None:
    coverage_flags = (
        "--cov-report term --cov-report xml:coverage.xml --cov tests "
        + " ".join([f"--cov {m}" for m in modules])
    )
    running_in_ci = os.getenv("CI") is not None
    hypothesis_profile = "ci" if running_in_ci else "dev"
    hypothesis_flags = f"--hypothesis-profile {hypothesis_profile}"
    xdist_flags = "--numprocesses auto --dist worksteal"
    cmd_call(
        "pytest --junitxml test-report.xml"
        f" {coverage_flags} {hypothesis_flags} {xdist_flags} tests/"
    )


def generate_fair_use_token_key() -> None:
    print(FairUseTokenService.generate_token_key())
