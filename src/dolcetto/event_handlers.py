# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from typing import Any, Callable

from litestar.config.app import AppConfig
from litestar.datastructures import State
from litestar.di import Provide
from pydantic import BaseModel

from dolcetto.openapi import operation_id_creator
from dolcetto.services import (
    FairUseTokenService,
    SmtpClient,
    get_logger,
    get_logging_config,
)
from dolcetto.settings import SecuritySettings, SmtpSettings

################################################################################
# Init
################################################################################


def _base_model_encoder(value: BaseModel) -> dict[str, Any]:
    return value.dict(by_alias=True)


def on_app_init(app_config: AppConfig) -> AppConfig:
    # Serialization
    app_config.type_encoders = {BaseModel: _base_model_encoder}
    # Config
    app_config.logging_config = get_logging_config()
    if app_config.openapi_config is not None:
        app_config.openapi_config.operation_id_creator = operation_id_creator
    # Settings injection
    app_config.dependencies["security_settings"] = Provide(
        SecuritySettings.load, use_cache=True, sync_to_thread=False
    )
    app_config.dependencies["smtp_settings"] = Provide(
        SmtpSettings.load, use_cache=True, sync_to_thread=False
    )
    # Services injection
    app_config.dependencies["logger"] = Provide(
        get_logger, use_cache=True, sync_to_thread=False
    )
    app_config.dependencies["fair_use_token_svc"] = Provide(
        FairUseTokenService, use_cache=True, sync_to_thread=False
    )
    app_config.dependencies["smtp_client"] = Provide(
        SmtpClient.create, use_cache=True, sync_to_thread=False
    )
    # Event handlers
    app_config.on_startup.insert(0, warn_if_security_disabled)
    return app_config


################################################################################
# Startup
################################################################################


def warn_if_security_disabled(
    _: State | None = None,
    logger_factory: Callable[..., logging.Logger] = get_logger,
    security_settings: SecuritySettings | None = None,
) -> None:
    security_settings = security_settings or SecuritySettings.load()
    if not security_settings.is_security_enabled():
        logger = logger_factory()
        logger.warning(
            "No API key has been configured,"
            " unauthenticated requests will be accepted"
        )
