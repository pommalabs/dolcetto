# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import hmac
import logging
import random
from datetime import datetime, timezone
from typing import Callable

from litestar import Request
from litestar.connection import ASGIConnection
from litestar.enums import ScopeType
from litestar.exceptions import NotAuthorizedException
from litestar.middleware import (
    AbstractAuthenticationMiddleware,
    AbstractMiddleware,
    AuthenticationResult,
    DefineMiddleware,
)
from litestar.types import (
    ASGIApp,
    HTTPResponseStartEvent,
    Message,
    Receive,
    Scope,
    Send,
)
from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor, SpanExporter
from opentelemetry.sdk.trace.sampling import Sampler
from opentelemetry.semconv.trace import SpanAttributes
from opentelemetry.trace import Span

from dolcetto.constants import API_KEY_HEADER_NAME
from dolcetto.services import get_exporter, get_logger, get_sampler
from dolcetto.settings import ApiKey, SecuritySettings


class ApiKeyAuthenticationMiddleware(AbstractAuthenticationMiddleware):
    def __init__(
        self,
        app: ASGIApp,
        logger_factory: Callable[..., logging.Logger],
        security_settings: SecuritySettings,
    ):
        super().__init__(app)

        self._logger_factory = logger_factory
        self._security_settings = security_settings
        self._security_enabled = security_settings.is_security_enabled()

    @staticmethod
    def define(
        logger_factory: Callable[..., logging.Logger] = get_logger,
        security_settings: SecuritySettings | None = None,
    ) -> DefineMiddleware:
        security_settings = security_settings or SecuritySettings.load()
        return DefineMiddleware(
            ApiKeyAuthenticationMiddleware,
            logger_factory=logger_factory,
            security_settings=security_settings,
        )

    async def authenticate_request(
        self, connection: ASGIConnection
    ) -> AuthenticationResult:
        if not self._security_enabled:
            # Security has been disabled. Therefore,
            # no check on API keys has to be performed.
            return AuthenticationResult(user="Anonymous", auth=None)

        request_api_key = self._get_request_api_key(connection)
        valid_api_keys = self._get_valid_api_keys()
        matched_api_key = self._match_api_key(request_api_key, valid_api_keys)

        logger = self._logger_factory()
        logger.info('API key "%s" validated', matched_api_key.name)

        return AuthenticationResult(
            user=matched_api_key.name, auth=matched_api_key.value
        )

    def _get_request_api_key(self, connection: ASGIConnection) -> str:
        if (
            API_KEY_HEADER_NAME not in connection.headers
            or not connection.headers[API_KEY_HEADER_NAME]
        ):
            raise NotAuthorizedException("Missing API key")

        return connection.headers[API_KEY_HEADER_NAME]

    def _get_valid_api_keys(self) -> list[ApiKey]:
        # Expired API keys should be removed from the list, in order to reduce
        # time required for validation.
        valid_api_keys = list(
            filter(
                lambda ak: ak.expires_at is None
                or ak.expires_at >= datetime.now(timezone.utc),
                self._security_settings.api_keys,
            )
        )

        # API keys order is randomized in order to handle cases where many keys
        # have been defined and last keys in the list would always take more time
        # to be validated.
        random.shuffle(valid_api_keys)

        return valid_api_keys

    def _match_api_key(
        self, request_api_key: str, valid_api_keys: list[ApiKey]
    ) -> ApiKey:
        # Step 1: look for an API key with received value.
        matched_api_key = next(
            filter(
                # When validating an API key, timing attacks can be mitigated
                # by avoiding to compare strings with the "==" operator.
                lambda ak: hmac.compare_digest(ak.value, request_api_key),
                valid_api_keys,
            ),
            None,
        )

        # Step 2: make sure that filtered API key has not expired.
        if matched_api_key is None:
            raise NotAuthorizedException("Invalid API key")

        return matched_api_key


class HttpTracingMiddleware(AbstractMiddleware):
    def __init__(
        self,
        app: ASGIApp,
        sampler: Sampler | None,
        exporter: SpanExporter | None,
        request_filter: Callable[[Request], bool] | None,
    ):
        super().__init__(app, scopes={ScopeType.HTTP})

        sampler = sampler or get_sampler()
        exporter = exporter or get_exporter()

        if not isinstance(trace.get_tracer_provider(), TracerProvider):
            tracer_provider = TracerProvider(sampler=sampler)
            trace.set_tracer_provider(tracer_provider)
            tracer_provider.add_span_processor(BatchSpanProcessor(exporter))

        self._tracer = trace.get_tracer(HttpTracingMiddleware.__name__)
        self._request_filter = request_filter or (lambda _: True)

    @staticmethod
    def define(
        sampler: Sampler | None = None,
        exporter: SpanExporter | None = None,
        request_filter: Callable[[Request], bool] | None = None,
    ) -> DefineMiddleware:
        return DefineMiddleware(
            HttpTracingMiddleware,
            sampler=sampler,
            exporter=exporter,
            request_filter=request_filter,
        )

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        request: Request = Request(scope)

        # Apply request filter: if it return False, then request should not be traced.
        if not self._request_filter(request):
            await self.app(scope, receive, send)
            return

        with self._tracer.start_as_current_span(
            "main", kind=trace.SpanKind.SERVER
        ) as span:
            self._add_request_attributes(request, span)

            async def send_wrapper(message: Message) -> None:
                if message["type"] == "http.response.start":
                    self._add_response_attributes(message, span)
                await send(message)

            await self.app(scope, receive, send_wrapper)

    def _add_request_attributes(self, request: Request, span: Span) -> None:
        span.set_attribute(SpanAttributes.HTTP_METHOD, request.method)
        span.set_attribute(SpanAttributes.HTTP_HOST, request.url.hostname or "")
        span.set_attribute(SpanAttributes.HTTP_ROUTE, request.url.path)
        span.set_attribute(SpanAttributes.HTTP_URL, str(request.url))

    def _add_response_attributes(
        self, message: HTTPResponseStartEvent, span: Span
    ) -> None:
        span.set_attribute(SpanAttributes.HTTP_STATUS_CODE, message["status"])
