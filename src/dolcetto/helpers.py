# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import re

SEPARATORS_CLEANUP_PATTERN = re.compile(r"[!#$%&'*+\-.^_`|~:]+")


def camelize(field_name: str) -> str:
    if len(field_name) == 0:
        return field_name
    field_name = field_name.title()
    field_name = field_name[0].lower() + field_name[1:]
    return SEPARATORS_CLEANUP_PATTERN.sub("", field_name)
