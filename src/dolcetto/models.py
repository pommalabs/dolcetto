# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from datetime import datetime
from enum import StrEnum

from pydantic import BaseModel, ConfigDict, Field, NameEmail

from dolcetto.helpers import camelize


def get_model_config(config: ConfigDict | None = None) -> ConfigDict:
    config = config or ConfigDict()
    # A model should be immutable. Mutable models should be explicitly marked as so
    # and the reason why they are mutable should be documented.
    config["frozen"] = True
    # Automatically generates field aliases by "camelCase"-ing field names.
    config["alias_generator"] = camelize
    # Even if fields have aliases, model population should use field names.
    config["populate_by_name"] = True
    return config


class AppVersion(BaseModel):
    """Application version."""

    model_config = get_model_config()

    version: str = Field(
        description="Application version.",
        min_length=1,
        pattern=r"^\d+(\.\d+)*(\+[0-9a-f]{8})?$",
        examples=["1.2+abcdef12"],
    )


class HealthCheckStatus(StrEnum):
    """The outcome of a health check run."""

    SUCCESS = "success"
    FAILURE = "failure"


class HealthCheckResult(BaseModel):
    """The result of an individual health check."""

    model_config = get_model_config()

    checker: str = Field(
        description="Name of checker (name of the function registered as a check).",
        examples=["ping"],
    )
    output: str = Field(
        description="Verbal description of status.",
        examples=["OK"],
    )
    passed: bool = Field(
        description="Boolean value to tell if check passed.",
    )
    timestamp: float = Field(
        description="A timestamp.",
        gt=0,
        allow_inf_nan=False,
        examples=[datetime.now().timestamp()],
    )
    expires: float = Field(
        description="A timestamp to tell when the check will expire."
        " This can be used to cache results.",
        gt=0,
        allow_inf_nan=False,
        examples=[datetime.now().timestamp()],
    )
    response_time: float = Field(
        description="A timestamp to tell when the check was run.",
        gt=0,
        allow_inf_nan=False,
        examples=[0.414222],
    )


class HealthCheckResponse(BaseModel):
    """A system health check response."""

    model_config = get_model_config()

    hostname: str = Field(
        description="Name of host server.",
        examples=["server"],
    )
    status: HealthCheckStatus = Field(
        description="Verbal description of status.",
    )
    timestamp: float = Field(
        description="A timestamp.",
        gt=0,
        allow_inf_nan=False,
        examples=[datetime.now().timestamp()],
    )
    results: list[HealthCheckResult] = Field(
        description="List of results. It should contain at least one.",
        min_length=1,
    )


class ApiKey(BaseModel):
    model_config = get_model_config()

    name: str = Field(
        description="Descriptive API key name",
        min_length=1,
        examples=["My API client"],
    )
    value: str = Field(
        description="API key value",
        min_length=1,
        examples=["My secret"],
    )
    expires_at: datetime | None = Field(
        default=None,
        description="Optional API key expiration timestamp.",
        examples=[datetime.now().isoformat()],
    )


class FairUseToken(BaseModel):
    """Fair use token, used to control communication on public endpoints."""

    model_config = get_model_config()

    token: str = Field(
        description="Encrypted fair use token.",
        examples=["base64EncodedString"],
    )


class MailMessage(BaseModel):
    """Mail message."""

    model_config = get_model_config()

    subject: str = Field(
        description="Subject.",
        min_length=1,
        examples=["Hello World"],
    )
    from_address: NameEmail | None = Field(
        default=None,
        description='Optional "From" address.',
        examples=["brian@example.com"],
    )
    to_addresses: list[NameEmail] = Field(
        default=[],
        description='List of "To" addresses.',
        examples=[["dennis@example.com"]],
    )
    plain_text_content: str | None = Field(
        default=None,
        description="Optional plain text content.",
        min_length=1,
        examples=["Have a nice day."],
    )
    html_content: str | None = Field(
        default=None,
        description="Optional HTML content.",
        min_length=1,
    )
