# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar import get
from litestar.response import Redirect
from litestar.status_codes import HTTP_302_FOUND


@get(["/", "/docs"], status_code=HTTP_302_FOUND, include_in_schema=False)
async def redirect_to_docs() -> Redirect:
    return Redirect("/schema/swagger")
