# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import tomllib


def _read_pyproject_field(field_name: str) -> str:
    with open("pyproject.toml", mode="rb") as pyproject_file:
        parsed_pyproject = tomllib.load(pyproject_file)
        return str(parsed_pyproject["tool"]["poetry"][field_name])


APP_DESCRIPTION = _read_pyproject_field("description")
APP_VERSION = _read_pyproject_field("version")

API_KEY_HEADER_NAME = "X-Api-Key"
