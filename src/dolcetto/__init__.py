# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from dolcetto.constants import API_KEY_HEADER_NAME, APP_DESCRIPTION, APP_VERSION
from dolcetto.controllers import AppInfoController
from dolcetto.event_handlers import on_app_init
from dolcetto.middleware import ApiKeyAuthenticationMiddleware, HttpTracingMiddleware
from dolcetto.openapi import (
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    MIT_LICENSE,
)
from dolcetto.route_handlers import redirect_to_docs
from dolcetto.services import logging_exception_handler

__all__ = [
    # Constants
    "API_KEY_HEADER_NAME",
    "API_KEY_SECURITY_SCHEME",
    "API_KEY_SECURITY_SCHEME_NAME",
    "APP_DESCRIPTION",
    "APP_VERSION",
    "MIT_LICENSE",
    # Middleware
    "ApiKeyAuthenticationMiddleware",
    "HttpTracingMiddleware",
    # Route handlers
    "AppInfoController",
    "redirect_to_docs",
    # Services
    "logging_exception_handler",
    "on_app_init",
]
