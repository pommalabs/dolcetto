# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import sys
from enum import StrEnum
from functools import cache
from ipaddress import IPv4Address, IPv6Address
from uuid import UUID

from pydantic import Field, NameEmail
from pydantic_settings import BaseSettings, SettingsConfigDict

from dolcetto.models import ApiKey


def get_settings_config(config: SettingsConfigDict | None = None) -> SettingsConfigDict:
    config = config or SettingsConfigDict()
    # Settings should be immutable.
    config["extra"] = "allow"
    config["frozen"] = True
    if "pytest" not in sys.modules:
        # Environment file should not be read when running under pytest.
        # That should avoid configuration mistakes and undesired external calls.
        config["env_file"] = ".env"
        config["env_file_encoding"] = "utf-8"
    return config


class SecuritySettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="security_"))

    api_keys: frozenset[ApiKey] = Field(
        default=frozenset(),
        description="API keys which could be used to perform security checks."
        " Each API key is described by three properties:"
        " a descriptive name, the key value, an optional expiration timestamp.",
    )
    fair_use_token_key: str | None = Field(
        default=None,
        description="Optional secret key used to encrypt fair use tokens."
        " If not specified, a random one will be generated: however,"
        " specifying a key is mandatory when multiple instances are active,"
        " because each instance needs to be able to decrypt tokens generated"
        " by other instances.",
        min_length=44,
        max_length=44,
    )
    fair_use_token_ttl: int = Field(
        default=3600,
        description="Time-to-live, specified in seconds, of fair use tokens."
        " Default TTL is 3600 seconds.",
        gt=0,
    )
    fair_use_token_cache_size: int = Field(
        default=256,
        description="How many validated fair use tokens should be stored"
        " inside an in-memory cache, which is used to verify that tokens"
        " are not sent again. Default size is 256.",
        gt=0,
    )

    @staticmethod
    @cache
    def load() -> "SecuritySettings":
        return SecuritySettings()

    def is_security_enabled(self) -> bool:
        return len(self.api_keys) > 0


class SmtpConnectionSecurity(StrEnum):
    NONE = "none"
    SSL = "ssl"
    STARTTLS = "starttls"


class SmtpSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="smtp_"))

    server: str | IPv4Address | IPv6Address = Field(
        default="localhost",
        description="Host name or IP address of the SMTP server.",
        min_length=1,
    )
    port: int = Field(
        default=0,
        description="SMTP server port.",
    )
    connection_security: SmtpConnectionSecurity = Field(
        description="If and how each SMTP connection should be secured."
        ' Set to "ssl" in order to open always encrypted connections using SSL.'
        ' Set to "starttls" in order to use opportunistic encryption with STARTTLS.'
        " Explicitly set to None in order to open unencrypted SMTP connections."
    )
    user_name: str | None = Field(
        default=None,
        description="The user name to authenticate with."
        " If not specified, SMTP login will not be performed.",
    )
    password: str | None = Field(
        default=None,
        description="The password for the authentication.",
    )
    default_from_address: NameEmail = Field(
        description='Default "From" address, used when it is not specified'
        ' in a given mailing. It is mandatory to specify a default "From" address'
        ' because it can be usually filled with a common "no-reply" address.'
    )
    default_to_addresses: list[NameEmail] = Field(
        default=[],
        description='Default "To" addresses, added to each mailing in addition'
        " to the ones specified in the mail message.",
    )

    @staticmethod
    @cache
    def load() -> "SmtpSettings":
        return SmtpSettings()  # type: ignore[call-arg]


class AppInsightsSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="appinsights_"))

    connection_string: str | None = Field(
        default=None,
        description="Connection string provided by Azure Application Insights.",
        min_length=1,
    )
    instrumentationkey: UUID | None = Field(
        default=None,
        description="Instrumentation key provided by Azure Application Insights."
        " Explicitly using instrumentation key has been deprecated by Microsoft."
        " Please use a connection string instead.",
    )

    @staticmethod
    @cache
    def load() -> "AppInsightsSettings":
        return AppInsightsSettings()

    def has_connection_string(self) -> bool:
        return bool(self.connection_string or self.instrumentationkey)

    def get_connection_string(self) -> str:
        if self.connection_string:
            return self.connection_string
        if self.instrumentationkey:
            return f"InstrumentationKey={self.instrumentationkey}"
        raise ValueError(
            "Neither connection string nor instrumentation key have been provided"
        )
