# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from contextlib import contextmanager
from datetime import timedelta
from typing import Generator

from hypothesis import settings
from pytest import FixtureRequest, LogCaptureFixture


def register_hypothesis_profiles() -> None:
    # Dev machines might have an high and unreliable timing due to many parallel tasks.
    settings.register_profile("dev", deadline=timedelta(milliseconds=5000))
    # CI builds are run on potentially slow or overloaded servers,
    # therefore tests have unreliable timing and a less tight deadline is needed.
    settings.register_profile("ci", deadline=timedelta(milliseconds=2000))


@contextmanager
def capture_logs_at_level(
    request: FixtureRequest, level: int
) -> Generator[LogCaptureFixture, None, None]:
    caplog: LogCaptureFixture | None = None
    try:
        caplog = LogCaptureFixture(request.node, _ispytest=True)
        # Fixture stashes captured logs in request node, which does not change
        # when Hypothesis executes the same test multiple times. Following clear
        # is needed to make sure that each test run starts from an empty stash.
        caplog.clear()
        with caplog.at_level(level):
            yield caplog
    finally:
        if caplog:
            caplog._finalize()  # pylint: disable=protected-access
